Purpose
===================
  I was looking for a decent drop-like navigation for my site. I didn't want
it to be too much, but I didn't want it to be any less either. I think this
one might be decent enough to fit in [my code](https://bitbucket.org/mrjohnson/srportal), and if not, well, I'll just
have to find another. -- Mr. Johnson.
